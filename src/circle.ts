import { serialize } from "@thi.ng/hiccup";

const W = 600, H = 800;

const rFloat = (min, max) => Math.random()*(max-min) + min;
const rInt = (min, max) => Math.floor(rFloat(min, max));
const cossin = (a, r) => [Math.cos(a)*r, Math.sin(a)*r];
const rep = (n, fn) => Array.from({length:n}, (_,i)=> fn(i));

const rad = rep(14, i=> 50+i*60 + rInt(-40, 40));

const spread = rFloat(0.6, 1);

const hue = rInt(0,360);

const thread = (ridx, sangle) => {
    const srad = rad[ridx];
    const erad = rad[ridx+1];

    const angSpread = spread ** ridx;
    const sp = cossin(sangle, srad);

    const ends = rep(10, _ => sangle + rFloat(-angSpread,angSpread));

    return ridx>= rad.length - 1 
        ? [] 
        : ["g", 
        
        ["circle", {cx: sp[0], cy: sp[1], r:2, fill:"white"}],
        ["g", {},
        ends.map(ea => {

            const endpt = cossin(ea, erad);
            return ["g", {opacity:0.2}, ["path", {
                d: `M ${sp} C ${ cossin(sangle, erad+rFloat(-10,10)) } ${cossin(ea, srad+rFloat(-10,10))} ${endpt}`,
            }],
            ["circle", {cx: endpt[0], cy: endpt[1], r:rFloat(1,4), fill:"white"}]]
        }),
        ends.filter(_ => Math.random() <0.1)
        .map(ea => thread(ridx+1, ea))
    ]]
}

const Y = rFloat(100-H,-100);

const svgTree = ["svg", {
    width:W,
    height:H,
    viewBox: [-W/2, Y, W, H]
},
    ["g", {stroke: `hsl(${hue}, ${rInt(50, 80)}%, 80%)`, fill:"none", },
    ["rect", {x:-W/2, y:Y, width:W, height:H, 
    stroke:"none",
    fill:`hsl(${hue}, ${rInt(10, 70)}%, 10%)`}],
    rep(rInt(20,100), _=>thread(0, rFloat(0, Math.PI)*2))

    ]    
];
document.body.insertAdjacentHTML("beforeend", serialize(svgTree));